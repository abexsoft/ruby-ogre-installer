# ruby-ogre-installer

Ruby-ogre-installer downloads a ruby-ogre gem from bitbucket downloads and installs it.  
The reason is that ruby-ogre gem is too large to push on rubygems.org,

## Installation

    $ gem install ruby-ogre-installer

