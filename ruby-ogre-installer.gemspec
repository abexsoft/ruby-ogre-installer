lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'ruby-ogre-installer/version'

Gem::Specification.new do |gem|
  gem.name          = "ruby-ogre-installer"
  gem.version       = Ruby::Ogre::Installer::VERSION
  gem.authors       = ["abexsoft"]
  gem.email         = ["abexsoft@gmail.com"]
  gem.summary       = %q{Ruby-ogre-installer downloads a ruby-ogre gem from bitbucket downloads and install it.}
  gem.description   = %q{Ruby-ogre gem is too large to push on rubygems.org.}
  gem.homepage      = ""
  gem.platform      = Gem::Platform::CURRENT
  gem.extensions    = ['Rakefile']

  gem.files         = Dir['Gemfile',
                          'LICENSE.txt',
                          'README.md',
                          'Rakefile',
                          'ruby-ogre-installer.gemspec',
                          'lib/**/*'
                         ]
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ["lib"]
end
